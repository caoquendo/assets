# Assets
This repository has a very rudimentary implementation of CSS assets to be served to Moodle pages.

## Serving Locally

Local consumers will hit the http://localhost:4500/public URL to get access to the compiled assets.

1. Run `python3 -m http.server 4500` from the root folder to launch the server.

## Develop

* Install a `node` version.
* Run `npm run live-dev` so that all the changes done to the files in `src` are automatically published to `public`.